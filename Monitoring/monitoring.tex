\documentclass[]{spie}  %>>> use for US letter paper
%\documentclass[a4paper]{spie}  %>>> use this instead for A4 paper
%\documentclass[nocompress]{spie}  %>>> to avoid compression of citations

\renewcommand{\baselinestretch}{1.0} % Change to 1.65 for double spacing
 
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\title{Monitoring the performance of the SKA CICD Infrastructure}

\author[a]{Di Carlo M.}
\author[b]{Harding P.}
\author[b]{Yilmaz U.}
\author[d]{Maia D.}
\author[c]{Ribeiro B.}
\author[c]{Nunes D.}
\author[c]{Regateiro D.}
\author[d]{Morgado J.B.}
\author[e]{Paulo M.}
\author[e]{Santos M.}
\author[a]{Dolci M.}

\affil[a]{INAF Osservatorio Astronomico d'Abruzzo, Teramo, Italy}
\affil[b]{SKA Observatory, Macclesfield, UK}
\affil[c]{Atlar Innovation, Portugal}
\affil[d]{CICGE, Faculdade de Ciências da Universidade do Porto, Portugal}
\affil[f]{Critical Software, Portugal}

\authorinfo{Further author information: (Send correspondence to Di Carlo M.)\\Di Carlo M.: E-mail: matteo.dicarlo@inaf.it\\  Dolci M.: E-mail: mauro.dolci@inaf.it\\ Harding P.: E-mail: P.Harding@skatelescope.org}

% Option to view page numbers
\pagestyle{empty} % change to \pagestyle{plain} for page numbers   
 
\begin{document} 
\maketitle

\begin{abstract}
The selected solution for monitoring the SKA CICD (continuous integration and continuous deployment) Infrastructure is Prometheus and Grafana. Starting from a study on the modifiability aspects of it, the Grafana project emerged as an important tool for displaying data in order to make specific reasoning and debugging of particular aspect of the infrastructure in place. Its plugin architecture easily allow to add new data sources like prometheus and the TANGO-controls framework related data sources has been added as well. The main concept of grafana is the dashboard, which enable to create real analysis. Thanos is also used for high availability, resilience and long term storage retention for monitoring data. In this paper the monitoring platform is presented which take advantage of different datasources and a variety of different panels (widget) for reasoning on archiving data, monitoring data, state of the system and general health of it.
\end{abstract}

% Include a list of keywords after the abstract 
\keywords{diagnostic, SKA, Prometheus, System team, monitoring, CICD}

\section{Introduction} 

The Square Kilometre Array (SKA) project has selected as development process the SAFe Agile (Scaled Agile framework) that is an incremental and iterative process with a specialized team (known as the Systems Team) devoted to supporting the Continuous Integration, Continuous Deployment (CICD)\cite{SKA-CICD}, test automation and quality. In the last 3 years the team has been busy with building an infrastructure for supporting the CICD toghether with a monitoring solution for it. 

The SKA infrastructure consists of a standard footprint of VPN/SSH JumpHost gateway (called Terminus), Monitoring, Logging, Storage and K8s services to support the GitLab\cite{gitlab} runner architecture, and MVP testing facilities as shown in Fig.\ref{fig:simplified-infra} used to support DevOps and Integration testing facilities. In specific, the logging solution selected is Elasticsearch\cite{elastcsearch}, the storage solution is Ceph\cite{ceph} and the (central) artefact repository (CAR) is Nexus\cite{nexus}. In relation to CAR, it is important to notice that only artefacts produced from the gitlab pipeline are allowed to be stored into the CAR and only if the pipeline has been triggered for a git tag. In all other cases, the gitlab artefact repository is used. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.8\columnwidth]{simplified infra}
	\caption{Simplified infrastructure}
	\label{fig:simplified-infra}
\end{figure}

\section{Prometheus} 

The selected monitoring solution is Prometheus. It is a client server architecture where prometheus represents the client which read (scrape in its language) timestamped information from many servers (called targets or exporters). All data are then stored in a disk in TSDB format~\cite{tsdb}. Fig.~\ref{fig:prometheus} reports a diagram that illustrates the monitoring architecture in SKA. The components of the diagram where the most important are: 
\begin{itemize}
    \item the prometheus server which is composed by the retrieval component, the TSDB storage, and the http server for information retrieval,
    \item the jobs/exporters components from which prometheus takes information as time series and 
    \item the data visualization and export which are external tools that integrates with prometheus with a specific query language called promql. 
\end{itemize}
Each exporter provides time series uniquely identified by its metric name and some optional key-value pairs (called labels). It is important to note that the exporter must give all the information about its monitoring that is it must have all the information related to the “instrument”. 

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=1.0\columnwidth]{prometheus_thanos.jpg}
   \caption{Detailed Monitoring Architecture}
   \label{fig:prometheus}
\end{figure}

\subsection{Exporters}
One of the most important quality of the prometheus monitoring solution is the modifiability. It is in fact very easy to add new exporter since they are basically simple http server which provide a well defined metrics information. 

In SKA, a set of commonly used exporters has been selected to be deployed (or enabled) in each node of the infrastructure. The most important are:
\begin{itemize}
	\item Node exporter\cite{node_exporter};
	\item Container runtime metrics: enabled depending of the runtime (containerd, docker or podman);
	\item kubernetes\cite{kubernetes} exporters with the help of kube-state-metrics\cite{kube-state-metrics} project;
	\item Elasticsearch metrics\cite{elastcsearch}
	\item Ceph metrics\cite{ceph}
\end{itemize}

There are also custom exporters since writing them is very easy. Since the monitoring and controls project is built on top of the TANGO-controls framework\cite{tango-controls}, one of the custom exporters made is the TANGO-exporter\cite{ska-tango-exporter} (Fig.~\ref{fig:exporter} shows the result of a query to this exporter). Other custom exporters collect metrics the logging infrastructure to check if logging is performed correctly in each node. 

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.3\columnwidth]{exporter}
   \caption{TANGO exporter graph view from Prometheus GUI}
   \label{fig:exporter}
\end{figure}

The configuration of the exporters in the Prometheus server happens with the help of a simple python script (prom-helper)\cite{ska-cicd-deploy-prometheus}. The script creates a process for 1) labelling each node in the openstack virtualization\cite{openstack} and 2) retrieving the labels and create the configuration file for the Prometheus server. The server run in a docker container and share a volume for the configuration files. The first step of the process happens every time a new node is added to the infrastructure while the second happen in a cron job of the node where the Prometheus container is running. 

\subsection{Alerts}

An important part of the monitoring solution selection is the ability to inform the maintainers (basically the system team) of any possible cause of problems. For this purpose, it has been adopted many alerts coming from various community (for examples from the project kubernetes-mixin\cite{kubernetes-mixin}). 
The alert manager sends alert messages to a slack channel\cite{slack} so that required (and for the moment) manual actions can be performed. Fig.\ref{fig:alerts} shows an example message. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.6\columnwidth]{alerts}
	\caption{An alert message in Slack}
	\label{fig:alerts}
\end{figure}

\section{Thanos}\label{thanos-section}
Thanos is a an open source project that provides global query view, high availability, long term data storage with historical lookup capabilities~\cite{thanos}. Thanos is used in conjunction with Prometheus instances and as a middla layer to provide its querying features to compose a centralised solution for monitoring. It has many components that are highlighted in Fig.~\ref{fig:prometheus} for replication, storage integration and centralised, load-balanced query functions. Its main components and how they are used in SKA are explained below. Note that, SKA is currently progressing towards implementing the architecture in Fig.~\ref{fig:prometheus}.

\begin{itemize}
	\item \textbf{Thanos Sidecar} is the main component which queries Prometheus, makes it available for querying and uploads TSDB blocks to object store. This is deployed next to each prometheus container where possible. If it's not possible to query it from outside, it's deployed elsewhere where it's configured to use Prometheus remote-write API to access the data.
	\item \textbf{Thanos Store Gateway} presents metrics from storage which is used for histrocial queries
	\item \textbf{Thanos Ruler} is the equivalent of Prometheus Ruler that is used for managing rules for Thanos
	\item \textbf{Thanos Compactor} is responsible for downsampling and retention for the data for effective long term storage
	\item \textbf{Thanos Querier} aggregates, deduplicates the underlying metrics data and implements Prometheus API and exposes it for dashboards and other tools
\end{itemize}

\section{Grafana}\label{grafana-section}
Another quality of Prometheus is its ability to integrate with with many visualization engines. One of them is Grafana~\cite{grafana}, an engine for displaying data on web coming from many data sources. Working with Grafana means to create dashboards in order to perform a particular analysis on a set of monitoring data. From an architectural point of view, it is a plugin architecture where a plugin can be: 
\begin{itemize}
    \item a panel (the basic visualization building block in Grafana),
    \item a data source (such as prometheus, mysql or elasticsearch) or 
    \item an app (combination of panels and data sources for a specific purpose).
\end{itemize}

This architecture can be personalized very well for giving the developers and testers the ability to diagnose any kind of problem that can happen in a production system. Fig.~\ref{fig:grafana-model} shows the resulting model (in uml, every box is an object) of the customization made. The starting point is the Grafana object which is basically an aggregation of plugins and is associated with one or more dashboards (a collection of panels). A plugin can be a panel, an app or a data source. The data sources used are Prometheus, mysql databases, one for the TANGO archiver~\cite{tangoarchiver} and another one for the TANGO database (which represents the CORBA naming server) and the elasticsearch database. 
In this model it is shown a simple customization made for displaying information in Grafana: the TANGO-Attribute\cite{ska-tango-attributes}) panel. 

Fig.~\ref{fig:panel-examples} shows some panels which displays different values from many different data sources.

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.5\columnwidth]{grafana-model}
	\caption{Grafana UML model}
	\label{fig:grafana-model}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.6\columnwidth]{panels example}
	\caption{Panels taken from the SKA grafana monitoring web application}
	\label{fig:panel-examples}
\end{figure}

\section{Dashboards}

All the selected tools for the SKA infrastructure come with one or more dashboards that helps in analysing some aspect of the entire CICD infrastructure. In this section we present some of the dashboard that we adopted from various data source together with two dashboard that has been created for SKA specific analysis. 
Fig. \ref{fig:node-exporter-dashboard}, fig. \ref{fig:elasticsearch-dashboard} and fig. \ref{fig:docker-exporter-dashboard} shows some dashboards adopted from the Grafana community for:
\begin{itemize}
	\item the node-exporter\cite{node-exporter-full}, 
	\item the health of the elasticsearch cluster\cite{elasticsearch-dashboard}, 
	\item the health of the docker containers\cite{docker-dashboard},
\end{itemize}

Fig. \ref{fig:tangodb-dashboard} shows a custom dashboard for monitoring the information inside the mysql TANGO-controls database. 

Recently kibana has been used for making some specific analysis like evaluating who are the producers of log messages that arrives to the log database from kubernetes namespaces. The analysis want to understand the cuase of a major problem in the elasticsearch cluster by understand why some specific shard were failing. For this purpose a kibana dashboard has been created and it is shown in fig. \ref{fig:kibana-dashboard}.

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.8\columnwidth]{node-exporter-dashboard}
       \caption{Node exporter dashboard}
   \label{fig:node-exporter-dashboard}
\end{figure}

\begin{figure}[!htb]
   \centering
   \includegraphics*[width=0.8\columnwidth]{elasticsearch dashboard}
       \caption{Elasticsearch dashboard for cluster health}
   \label{fig:elasticsearch-dashboard}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.8\columnwidth]{docker-exporter-dashboard}
	\caption{Docker metrics dashboard for container health}
	\label{fig:docker-exporter-dashboard}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.8\columnwidth]{tangodb-dashboard}
	\caption{TANGO-controls database dashboard}
	\label{fig:tangodb-dashboard}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.8\columnwidth]{kibana dashboard}
	\caption{Kibana dashboard}
	\label{fig:kibana-dashboard}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=0.8\columnwidth]{k8s-dashboard}
	\caption{Kubernetes pods dashboard per node}
	\label{fig:k8s-dashboard}
\end{figure}

Fig. \ref{fig:k8s-dashboard} shows a dashbord taken from the project kubernetes-mixin\cite{kubernetes-mixin}. 

\section{Conclusion and future work}
This paper presented an overview of the tools selected by the SKA project in order to monitor the performance of the infrastructure put in place for CICD purposes. In specific Prometheus with Thanos and Grafana have been selected for this scope. 
The main rationale for this choice is the modifiability aspect of both of them. In fact adding new exporters for Prometheus or adding a new dashboard is very easy. Besides the community around those two projects is very big and active and this gives enough trust on them.  
The potential of this diagnostic tool resides overall on the analysis that it enables. Considering the work done for "Achieving a rolled-up view of SKA TM health status and state: definition and analysis of aggregation methods"~\cite{10.1117/12.2314056}, and considering that each aspect of the lifecycle of every SKA element will be reported as timestamped data in a Prometheus server or in one of the data sources available in the TANGO framework, the aggregation methods provided in the paper can be easily realized. 

The metrics and dashboards gathered should be avaiable for a long time (at least for the duration of an observation) and should be available from different parts of world for developers, scientists and engineers. The architecture explained is here are in its trial phase while it's getting integrated completely to make this possible and will be investigated for performance, reliability and maintainance aspects. 

\acknowledgments % equivalent to \section*{ACKNOWLEDGMENTS}       
 
This work has been supported by Italian Government (MEF - Ministero dell'Economia e delle Finanze, MIUR - Ministero dell'Istruzione, dell'Università e della Ricerca).

% References
\bibliography{report} % bibliography data in report.bib
\bibliographystyle{spiebib} % makes bibtex use spiebib.bst

\end{document} 
