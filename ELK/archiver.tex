\documentclass[]{spie}  %>>> use for US letter paper
%\documentclass[a4paper]{spie}  %>>> use this instead for A4 paper
%\documentclass[nocompress]{spie}  %>>> to avoid compression of citations

\renewcommand{\baselinestretch}{1.0} % Change to 1.65 for double spacing
 
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}

\title{Using Elasticsearch for archiving with TANGO-controls framework}

\author[a]{Di Carlo M.}
\author[1]{Dolci M.}
\affil[a]{INAF Osservatorio Astronomico d'Abruzzo, Teramo, Italy}

\authorinfo{Further author information: Di Carlo M.: E-mail: matteo.dicarlo@inaf.it}

% Option to view page numbers
\pagestyle{empty} % change to \pagestyle{plain} for page numbers   
\setcounter{page}{301} % Set start page numbering at e.g. 301
 
\begin{document} 
\maketitle

\begin{abstract}
The TANGO controls framework community has put a lot of effort in creating the HDB++ software system that is an high performance, event-driven archiving system. Its design allows storing data into traditional database management systems such as MySQL as well as NoSQL database such as Apache Cassandra. The architecture allow also to easily extend it to other noSql database like, for instance, Elasticsearch. This paper describes the extension for Elasticsearch made and how to use it alongside its graphical tool called Kibana.
\end{abstract}

% Include a list of keywords after the abstract 
\keywords{TANGO, HDB++, Elasticsearch, ELK, noSql DB}

\section{Introduction to TANGO-controls and HDB++}
The tango controls system is built on top of the CORBA (Common Object Request Broker Architecture\cite{Corba}) standard, limiting the possibility to introduce new objects with the IDL (interface definition language) to the “device” concept, in order to build distributed control system. Therefore, a device is mainly an object with attributes within a process called “device server”\cite{tango-controls}.
One of its core feature is the event model, which allows the communication between devices according to a predefined event. One of the event typology available is the archive event, that can be configured to be fired with an absolute or relative change in value (for a particular attribute of a device) or it can be configured to archive in a polling fashion\cite{hdbintro}.

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{hdb runtime.png}
	\caption{HDB++ Runtime View.}
	\label{fig:hdb_runtime_view}
\end{figure}

Fig. \ref{fig:hdb_runtime_view}(available on the online documentation\cite{hdbintro}) shows the main runtime components of the HDB+ system that are the Configuration Manager and the Event Subscriber (aka Archiver). The first one is a device that assists in adding, modifying, moving, deleting an attribute to/from the archiving system while the second one is the receiver of the event, the one that has to store the value of the configured attribute.

\subsection{Abstract DB}
Fig. \ref{fig:hdb_module_view} shows the architecture of the HDB++ in term of modules (mainly c++ library). The two main module are the “Event Subscriber” and the “Configuration Manager”. Both of them use a third module, called “Database Abstraction Layer”, that define two interfaces (namely DBFactory and AbstractDB) for extending the archiving system to other database system. Extending the HDB++ system, therefore, is very easy: it is only needed to implement the above two interfaces. 
The figure shows only two implementations of the abstraction layer that allows the interaction (for storage purpose) to the Mysql database system\cite{Mysql} and the Cassandra database system\cite{Cassandra} but other implementation are available such as TimescaleDB\cite{libhdbpp-timescale}. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{module view.png}
	\caption{HDB++ Module View.}
	\label{fig:hdb_module_view}
\end{figure}

\subsection{Data Model}

The tradeoff of the simplicity of extending the system is a fixed data model, which is the schema of the database. This means that the archiving interfaces (both the “Event Subscriber” and the “Configuration Manager”) cannot be changed and what is archived must always have the same shape. In essence, it consists of five tables, shown in Figure \ref{fig:hdb_data_model}, which stores the following information:
\begin{itemize}
	\item AttributeConfiguration: for each attribute (to store in DB when needed) there is an entry in this table which associate it with a specific data type;
	\item AttributeConfigurationHistory: for each operation on attributes (add/remove/start/stop), a row is inserted with the timestamp of the operation;
	\item AttributeParameter: for each attribute (to store in DB when needed) there is an entry in this table which associate it with the list of parameter already stored in the TANGO database;
	\item AttributeEventData: for each event there is a row which stores the information associated with the event (mainly timing and quality factor);
	\item Value: for each event stored in the AttributeEventData, there is an entry in a Value table (double, long, string and so on).
\end{itemize}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{hdb data model.png}
	\caption{HDB++ Data Model.}
	\label{fig:hdb_data_model}
\end{figure}

\section{Introduction to Elasticsearch}

Elasticsearch\cite{ELK} is a real-time distributed search and analytics engine. The term “real-time” refers to the ability to search (and sometimes create) data as soon as they are produced; traditionally, in fact, web search crawls and indexes web pages periodically, returning results based on relevance to the search query. It is distributed because its indices are divided into shards with zero or more replicas. But the main ability is the analytics engine which allows the discovery, interpretation, and communication of meaningful patterns in data. 
It is based on Apache Lucene\cite{lucene}, a free and open-source information retrieval software library, therefore Elasticsearch is very good for full text search (like for logging data or text files in general). It is developed alongside a data-collection and log-parsing engine called Logstash, and an analytics and visualization platform called Kibana. The three products are designed for use as an integrated solution, referred to as the "Elastic Stack" (formerly the "ELK stack").   

\subsection{Elasticsearch as noSQL database}

Looking at the definition given in \citenum{ELK}, it is not straightforward to assert that Elasticsearch is a noSql Database; the problem is the definition of noSql which it is not very precise: “Next Generation Databases mostly addressing some of the points: being non-relational, distributed, open-source and horizontally scalable"\cite{noSql}. 
Since that definition is not exact, the only possibility is to summarize the main features of Elasticsearch in order to understand its qualities. They are: 
\begin{itemize}
	\item no transaction: no support for transaction; 
	\item schema flexible: there is no need to specify the schema upfront;
	\item relations: denormalization, parent-child relations and nested objects;
	\item robustness: to properly work, elasticsearch requires that memory is abundant;
	\item distributed: it is a CP-system in the CAP (Consistency-Availability-Partition tolerance) theorem\cite{cap};
	\item no security: there is no support for authentication and authorization. 
\end{itemize}

When using a software like Apache Lucene or Elasticsearch, all the features must be well considered and, also, an important consideration deserves the relations.
ELK see data as everything is flat: basically every document, stored in ELK, is independent and therefore every document should contain all of the information required to decide whether it matches a query. In particular, this helps in indexing, in searching and in scalability since documents can be spread across multiple nodes. 

\section{HDB++ library for Elasticsearch}

The selected development language was C++ mainly because HDB++ is made in that language. It includes the ability to work with REST\cite{REST} and with Json data\cite{JSON}. The details of the implementation can be found at \citenum{libhdbpp-elk} and \citenum{hdbelk}.

Fig. \ref{fig:hdbelk_class_diagram} shows the class diagram of the implementation done. The central class is the ElasticDB which realizes the main methods of the interface AbstractDB (that uses the classes HdbEventDataType, EventData and AttrConfEventData belonging to the TANGO core library). The classes AttributeName and Log are used to extract the information needed from the full attribute name and to log data messages. The class DAL (refer to Data Access Layer\cite{DAL}) provides to the ElasticDB class a simple access to the Elasticsearch functionalities. In fact, it implements four private methods which represents the basic operation (except for the delete operation) that can be done with a database (Insert, Update, Get single entity and search for entities). 

With the help of those simple methods and for each information to store, there are one or more method to save and read the data. For example, if a new archive event arrives to the system (represented with the class EvenData), the ElasticDB will create the object (in this case an instance of EventData) and simply pass it to the DAL which will store it. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{hdb elastic.png}
	\caption{HDB++ library for Elasticsearch class diagram.}
	\label{fig:hdbelk_class_diagram}
\end{figure}

\section{Testing}
Compared to the work done in \citenum{hdbelk}, this work represents an improvement in two aspects: testing and containerization. 
In specific, it has been evaluated two main options for testing. The first one is the same used for the HDB++ library for Apache Cassandra backend\cite{Cassandra} called Catch2\cite{Catch2}. The second option is Google Test\cite{gtest}. 
While both are good options for C++ testing, the selection was for Google Tests because it is based on the popular xUnit architecture. In specific, tests are independent, repeatable, well organized, portable, reusable and when it fails should provide as much information about the problem as possible. It is also important to consider that there is a different terminology:
\begin{itemize}
	\item Google test case === test suite === group of tests;
	\item Google Test === Test === C++ macro TEST();
	\item Google test fixture class: when grouping tests that share common objects.
\end{itemize}

Fig. \ref{fig:test_examples} and fig. \ref{fig:gtest_configuration} shows respectively an example of a test made with the selected testing framework and the configuration made for CMake. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{tests example.png}
	\caption{Test examples.}
	\label{fig:test_examples}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{gtest configuration.png}
	\caption{Google tests configuration.}
	\label{fig:gtest_configuration}
\end{figure}

\section{Containerization}

As said above, together with testing the main improvement of this paper to the one presented in \citenum{hdbelk} is the contanerization done. 
In specific, a Dockerfile has been developed which include all the necessary dependencies for the archiver device such as:
\begin{itemize}
	\item the Interface library for the HDB++\cite{libhdbpp},
	\item the TANGO-controls framework dependencies\cite{tango-controls},
	\item the ReST client for C++\cite{restclient-cpp},
	\item JSON for Modern C++\cite{nlohmann},
	\item Tango device server for the HDB++ Event Subscriber\cite{hdbpp-es},
	\item Tango device server for the HDB++ Configuration Manager\cite{hdbpp-cm}.
\end{itemize}

Most of the dependencies are included with the live at head philosophy\cite{lhead}. Therefore the project refers to the main branch of the archiver libraries such as the Event subscriber and the configuration manager. This philosophy has been selected for two main reasons: the first one is to avoid the handling of multiple versions for those projects and secondary because that old versions appears to be not working perfectly compared to the main branches. 

The first and most important result of the containerization work is that it is possible to have a reproducible environment in many different contest. For instance, thanks to Gitlab\cite{gitlab}, it was possible to realize a pipeline that build, test and publish a container image which garuantee a very good confidence in term of reliability. Fig. \ref{fig:gitlab pipeline} and fig. \ref{fig:gitlab test job} shows the gitlab pipeline made available for the project and the test job which shows how the image built in the previous steps of the pipeline is used in the test job. 

\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{gitlab pipeline.png}
	\caption{Gitlab Pipeline.}
	\label{fig:gitlab pipeline}
\end{figure}


\begin{figure}[!htb]
	\centering
	\includegraphics*[width=.6\columnwidth]{gitlab test job.png}
	\caption{Gitlab Test Job.}
	\label{fig:gitlab test job}
\end{figure}

Together with the Dockerfile it has been developed an integration with kubernetes\cite{Kubernetes} and helm chart\cite{Helm} in order to manage the installation of the HDB++ and the Elasticsearch database. In specific it has been used widely the resources available from the SKA repositories such as the SKA Tango charts utility\cite{ska-tango-charts} (ska-tango-base and ska-tango-util)

In order to take the full advantage of the Gitlab pipeline, a Makefile has been developed which allows, with only one command, to: 
\begin{itemize}
	\item Build or test the C++ library locally;
	\item build and push the docker image of the project;
	\item run all tests in a container and start an elasticsearch database in a container;
	\item install, uninstall or reinstall the application using Helm\cite{Helm}.
\end{itemize}

\section{Conclusion}
This work is mainly an extension of what presented in \citenum{hdbelk} for testing and containerization. In specific a new Docker image and helm chart have been created with a live at head philosophy\cite{lhead}; tests have been made with google and the gitlab pipeline has been enabled so that the building, the testing and, in case of request, the publish can ben done. 

\acknowledgments % equivalent to \section*{ACKNOWLEDGMENTS}       
 
This unnumbered section is used to identify those who have aided the authors in understanding or accomplishing the work presented and to acknowledge sources of funding.  

% References
\bibliography{report} % bibliography data in report.bib
\bibliographystyle{spiebib} % makes bibtex use spiebib.bst

\end{document} 
