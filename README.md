# SPIE2022

Papers for SPIE 2022 - Astronomical telescopes + Instrumentation, Montreal Canada

## Usage

In order to make a contribution, please create a branch and a merge request with all the changes you think are important to do. 

There are 3 papers in this repository all of them are tex file that can be compiled with any tex compiler. 
The CICD folder contains the paper for the continuous integration continuos deployment and the first draft of the oral presentation (in power point).
The monitoring folder contains the paper for the prometheus architecture and the first draft of the poster (in power point).
The ELK folder contains the update the tango-archiver (to update from the last ICALEPCS). 

## Windows

Use tex studio: https://www.texstudio.org/